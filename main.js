/*code to accompany assingment07*/

$(document).ready(function(){
    "use strict";
    
/*hide footer*/    
    $("footer a").click(function(){
        $("footer").hide() 
    })
/*move car*/
    $("#car").css("position", "relative")
    $("#car").click(function(){
        $("#car").animate({left: '+=720'}, 1000);
    });
/*fade in and fade out ghost*/    
    $('#container p a').click(function(){
        $("#ghost").fadeToggle(3000) 
    });       
});
/* click to change theme to Fall*/    
function setFallTheme() {
    "use strict";
    $("body").css("background-color", "#D3998C");
    $("nav").css("background-color", "#480707");
    $("footer").css("background-color", "#F7CFC6");
};
/*click to change theme to Spring*/
function setSpringTheme() {
    "use strict";
    $("body").css("background-color", "#D8ECC3");
    $("nav").css("background-color", "#1C4905");
}; 
               


  
  
  
  
  
